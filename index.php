<?php

/**
 * Dayrui Website Management System
 *
 * @since	    version 3.0.0
 * @author	    Dayrui <dayrui@gmail.com>
 * @license     http://www.dayrui.com/license
 * @copyright   Copyright (c) 2011 - 9999, Dayrui.Com, Inc.
 * @filesource	  svn://www.dayrui.net/v3/index.php
 */

header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);
// 显示错误提示
if (function_exists('ini_set')) {
    ini_set('display_errors', TRUE);
    ini_set('memory_limit', '1024M');
}
// 查询执行超时时间
if (function_exists('set_time_limit')) {
    set_time_limit(0);
}
// 该文件的名称
if (!defined('SELF')) {
    define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
}
// 网站根目录
if (!defined('FCPATH')) {
    define('FCPATH', __DIR__.'/');
}

require FCPATH.'dayrui/laravel/bootstrap/autoload.php';

$app = require_once FCPATH.'dayrui/laravel/bootstrap/app.php';

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);